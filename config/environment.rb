require 'rubygems'
require 'bundler/setup'
require 'tilt'
require 'rabl'
require 'bcrypt'
require 'geocoder'
require 'rabl/template'
require 'lotus/setup'
require_relative '../lib/placetr'
require_relative '../apps/placetr/application'

Lotus::Container.configure do
  mount Placetr::Application, at: '/api/v1'
end
