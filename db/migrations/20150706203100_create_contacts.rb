Sequel.migration do
  change do
    create_table :contacts do
      uuid :id, null: false, default: Sequel.function(:uuid_generate_v4), primary_key: true
      String :first_name, null: false
      String :last_name
      String :site
      String :phone, null: false
      Time :created_at, null: false, default: 'now()'
      Time :updated_at, null: false, default: 'now()'
    end
  end
end
