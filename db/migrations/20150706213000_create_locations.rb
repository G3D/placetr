Sequel.migration do
  change do
    create_table :locations do
      uuid :id, null: false, default: Sequel.function(:uuid_generate_v4), primary_key: true
      uuid :user_id, null: false
      uuid :contact_id, null: false
      uuid :country_id, null: false
      Float :latitude, null: false
      Float :longitude, null: false
      String :name, null: false
      Boolean :verified, default: false
      String :description
      String :address
      String :city
      Time :created_at, null: false, default: 'now()'
      Time :updated_at, null: false, default: 'now()'
    end
  end
end
