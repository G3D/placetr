Sequel.migration do
  change do
    create_table :users do
      uuid :id, null: false, default: Sequel.function(:uuid_generate_v4), primary_key: true
      String :email, null: false
      String :authentication_token
      String :encrypted_password, null: false
      String :reset_password_token
      String :current_sign_in_ip
      String :last_sign_in_ip
      String :role, default: 'user'
      String :confirmation_token
      Integer :sign_in_count
      Time :reset_password_sent_at
      Time :current_sign_in_at
      Time :last_sign_in_at
      Time :confirmed_at
      Time :confirmation_sent_at
      Time :created_at, null: false, default: 'now()'
      Time :updated_at, null: false, default: 'now()'
    end
  end
end
