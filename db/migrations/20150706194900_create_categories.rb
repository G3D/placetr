Sequel.migration do
  change do
    create_table :categories do
      uuid :id, null: false, default: Sequel.function(:uuid_generate_v4), primary_key: true
      String :slug, null: false
      String :name
      Time :created_at, null: false, default: 'now()'
      Time :updated_at, null: false, default: 'now()'
    end
  end
end
