Sequel.migration do
  change do
    create_table :guides do
      uuid :id, null: false, default: Sequel.function(:uuid_generate_v4), primary_key: true
      uuid :user_id, null: false
      Float :latitude, null: false
      Float :longitude, null: false
      String :name, null: false
      String :description
      String :city
      Time :created_at, null: false, default: 'now()'
      Time :updated_at, null: false, default: 'now()'
    end
  end
end
