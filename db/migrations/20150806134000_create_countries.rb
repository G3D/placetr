Sequel.migration do
  change do
    create_table :countries do
      uuid :id, null: false, default: Sequel.function(:uuid_generate_v4), primary_key: true
      String :name, null: false
      String :country_code, null: false
      Time :created_at, null: false, default: 'now()'
      Time :updated_at, null: false, default: 'now()'
    end
  end
end
