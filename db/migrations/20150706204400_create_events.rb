Sequel.migration do
  change do
    create_table :events do
      uuid :id, null: false, default: Sequel.function(:uuid_generate_v4), primary_key: true
      uuid :location_id, null: false
      Time :start_time, null: false, default: 'now()'
      Time :end_time
      String :name, null: false
      String :description
      Time :created_at, null: false, default: 'now()'
      Time :updated_at, null: false, default: 'now()'
    end
  end
end
