# Configure your routes here
# See: http://www.rubydoc.info/gems/lotus-router/#Usage
#
# This route will look for `Placetr::Controllers::Home::Index` action in `apps/placetr/controllers/home/index.rb`.
# Please, uncomment the following line to have a working example.
# get '/', to: 'home#index'

resources 'countries', only: [:index]
resources 'sessions', only: [] do
  collection do
    post 'sign_in'#, to 'sessions#sign_in'
    delete 'sign_out'#, to 'sessions#sign_out'
  end
end
resources 'categories', only: [:index, :show, :create, :update, :destroy]
resources 'events', only: [:index, :show, :create, :update, :destroy]
resources 'contacts', only: [:index, :show, :create, :update, :destroy]
resources 'locations', only: [:index, :show, :create, :update, :destroy]
resources 'guides', only: [:index, :show, :create, :update, :destroy]
resources 'users', only: [:index, :show, :create, :update, :destroy]
