module Placetr::Controllers::Contacts
  class Update
    include Placetr::Action

    expose :contact

    params do
      param :id, presence: true
      param :first_name, presence: false
      param :last_name, presence: false
      param :site, presence: false
      param :phone, presence: false
    end

    def call(params)
      self.format = :json

      options = prepare_options(params)
      puts "Update contact #{options.inspect}"
      @contact = ContactRepository.find(options.delete('id'))
      options.each do |key, value|
        @contact.public_send("#{key}=", value)
      end
      ContactRepository.persist(@contact)
    end
  end
end
