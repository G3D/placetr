module Placetr::Controllers::Contacts
  class Show
    include Placetr::Action

    expose :contact

    params do
      param :id, presence: true
    end

    def call(params)
      self.format = :json
      @contact = ContactRepository.find(params[:id])
    end
  end
end
