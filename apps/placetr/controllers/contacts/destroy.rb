module Placetr::Controllers::Contacts
  class Destroy
    include Placetr::Action

    expose :contact

    params do
      param :id, presence: true
    end

    def call(params)
      self.format = :json

      @contact = ContactRepository.find(params['id'])
      ContactRepository.delete(@contact)
    end
  end
end
