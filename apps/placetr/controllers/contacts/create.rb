module Placetr::Controllers::Contacts
  class Create
    include Placetr::Action

    expose :contact

    params do
      param :first_name, presence: true
      param :last_name, presence: false
      param :site, presence: false
      param :phone, presence: true
    end

    def call(params)
      self.format = :json
      @contact = ContactRepository.persist(Contact.new(params))
    end
  end
end
