module Placetr::Controllers::Contacts
  class Index
    include Placetr::Action

    expose :contacts

    params do
      param :limit, presence: true
      param :offset, presence: true
    end

    def call(params)
      self.format = :json
      @contacts = ContactRepository.paginate(params['limit'], params['offset'])
    end
  end
end
