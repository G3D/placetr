module Placetr::Controllers::Guides
  class Create
    include Placetr::Action

    expose :guide

    params do
      param :user_id, presence: true
      param :latitude, presence: true
      param :longitude, presence: true
      param :name, presence: true
      param :description, presence: false
      param :city, presence: false
    end

    def call(params)
      self.format = :json
      @guide = GuideRepository.persist(Guide.new(params))
    end
  end
end
