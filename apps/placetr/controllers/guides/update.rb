module Placetr::Controllers::Guides
  class Update
    include Placetr::Action

    expose :guide

    params do
      param :id, presence: true
      param :user_id, presence: false
      param :latitude, presence: false
      param :longitude, presence: false
      param :name, presence: false
      param :description, presence: false
      param :city, presence: false
    end

    def call(params)
      self.format = :json

      options = prepare_options(params)
      puts "Update guide #{options.inspect}"
      @guide = GuideRepository.find(options.delete('id'))
      options.each do |key, value|
        @guide.public_send("#{key}=", value)
      end
      GuideRepository.persist(@guide)
    end
  end
end
