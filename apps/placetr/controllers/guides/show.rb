module Placetr::Controllers::Guides
  class Show
    include Placetr::Action

    expose :guide

    params do
      param :id, presence: true
    end

    def call(params)
      self.format = :json
      @guide = GuideRepository.find(params[:id])
    end
  end
end
