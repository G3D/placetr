module Placetr::Controllers::Guides
  class Index
    include Placetr::Action

    expose :guides

    params do
      param :limit, presence: true
      param :offset, presence: true
      param :radius, presence: false
      param :location do
        param :latitude, presence: false
        param :longitude, presence: false
        param :address, presence: false
      end
    end

    def call(params)
      self.format = :json
      options = prepare_options(params)
      options['radius'] = 1000 unless options['radius'].present?
      collection = geosearch(options)
      @total_size = collection.all.count
      @guides = collection.paginate(options['limit'], options['offset'])
    end

    def repository
      GuideRepository
    end
  end
end
