module Placetr::Controllers::Countries
  class Index
    include Placetr::Action

    expose :countries

    params do
      param :limit, presence: true
      param :offset, presence: true
    end

    def call(params)
      self.format = :json
      @countries = CountryRepository.paginate(params['limit'], params['offset'])
    end
  end
end
