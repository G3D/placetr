module Placetr::Controllers::Locations
  class Destroy
    include Placetr::Action

    expose :location

    params do
      param :id, presence: true
    end

    def call(params)
      self.format = :json

      @location = LocationRepository.find(params['id'])
      LocationRepository.delete(@location)
    end
  end
end
