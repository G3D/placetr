module Placetr::Controllers::Locations
  class Update
    include Placetr::Action

    expose :location

    params do
      param :id, presence: true
      param :user_id, presence: false
      param :contact_id, presence: false
      param :country_id, presence: false
      param :latitude, presence: false
      param :longitude, presence: false
      param :name, presence: false
      param :verified, presence: false
      param :description, presence: false
      param :address, presence: false
      param :city, presence: false
    end

    def call(params)
      self.format = :json

      options = prepare_options(params)
      puts "Update location #{options.inspect}"
      @location = LocationRepository.find(options.delete('id'))
      options.each do |key, value|
        @location.public_send("#{key}=", value)
      end
      LocationRepository.persist(@location)
    end
  end
end
