module Placetr::Controllers::Locations
  class Show
    include Placetr::Action

    expose :location

    params do
      param :id, presence: true
    end

    def call(params)
      self.format = :json
      @location = LocationRepository.find(params[:id])
    end
  end
end
