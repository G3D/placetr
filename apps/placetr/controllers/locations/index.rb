module Placetr::Controllers::Locations
  class Index
    include Placetr::Action
    include ::GeosearchHelper

    expose :locations

    params do
      param :limit, presence: true
      param :offset, presence: true
      param :radius, presence: false
      param :location do
        param :latitude, presence: false
        param :longitude, presence: false
        param :address, presence: false
      end
    end

    def call(params)
      self.format = :json
      options = prepare_options(params.to_hash)
      options['radius'] = 1000 unless options['radius'].present?
      collection = geosearch(options)
      @total_size = collection.all.count
      @locations = collection.paginate(options['limit'], options['offset'])
    end

    def repository
      LocationRepository
    end
  end
end
