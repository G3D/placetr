module Placetr::Controllers::Locations
  class Create
    include Placetr::Action

    expose :location

    params do
      param :user_id, presence: true
      param :contact_id, presence: true
      param :country_id, presence: true
      param :latitude, presence: true
      param :longitude, presence: true
      param :name, presence: true
      param :verified, presence: false
      param :description, presence: false
      param :address, presence: false
      param :city, presence: false
    end

    def call(params)
      self.format = :json
      @location = LocationRepository.persist(Location.new(params))
    end
  end
end
