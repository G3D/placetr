module Placetr::Controllers::Users
  class Show
    include Placetr::Action

    expose :user

    params do
      param :id, presence: true
    end

    def call(params)
      self.format = :json
      @user = UserRepository.find(params[:id])
    end
  end
end
