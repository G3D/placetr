module Placetr::Controllers::Users
  class Index
    include Placetr::Action

    expose :users

    params do
      param :limit, presence: true
      param :offset, presence: true
    end

    def call(params)
      self.format = :json

      @users = UserRepository.paginate(options['limit'], options['offset'])
    end
  end
end
