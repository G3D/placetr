module Placetr::Controllers::Users
  class Destroy
    include Placetr::Action

    expose :user

    params do
      param :id, presence: true
    end

    def call(params)
      self.format = :json

      @user = UserRepository.find(params['id'])
      UserRepository.delete(@user)
    end
  end
end
