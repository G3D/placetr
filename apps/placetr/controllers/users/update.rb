module Placetr::Controllers::Users
  class Update
    include Placetr::Action
    include ::BCrypt

    expose :user

    params do
      param :id, presence: true
      param :email, presence: false
      param :password, presence: false
      param :role, presence: false
    end

    def call(params)
      self.format = :json

      options = prepare_options(params)
      options['encrypted_password'] = BCrypt::Password.create(options.delete('password')) if options['password'].present?
      puts "Update user #{options.inspect}"
      @user = UserRepository.find(options.delete('id'))
      options.each do |key, value|
        @user.public_send("#{key}=", value)
      end
      UserRepository.persist(@user)
    end
  end
end
