module Placetr::Controllers::Users
  class Create
    include Placetr::Action
    include ::BCrypt

    expose :user

    params do
      param :email, presence: true
      param :password, presence: true
      param :role, presence: false
    end

    def call(params)
      self.format = :json
      options = prepare_options(params)
      options['encrypted_password'] = BCrypt::Password.create(options.delete('password'))
      options['authentication_token'] = SecureRandom.hex
      @user = UserRepository.persist(User.new(options))
    end
  end
end
