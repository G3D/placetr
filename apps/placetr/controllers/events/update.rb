module Placetr::Controllers::Events
  class Update
    include Placetr::Action

    expose :event

    params do
      param :id, presence: true
      param :location_id, presence: false
      param :start_time, presence: false
      param :end_time, presence: false
      param :name, presence: false
      param :description, presence: false
    end

    def call(params)
      self.format = :json

      options = prepare_options(params)
      puts "Update event #{options.inspect}"
      @event = EventRepository.find(options.delete('id'))
      options.each do |key, value|
        @event.public_send("#{key}=", value)
      end
      EventRepository.persist(@event)
    end
  end
end
