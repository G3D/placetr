module Placetr::Controllers::Events
  class Destroy
    include Placetr::Action

    expose :event

    params do
      param :id, presence: true
    end

    def call(params)
      self.format = :json

      @event = EventRepository.find(params['id'])
      EventRepository.delete(@event)
    end
  end
end
