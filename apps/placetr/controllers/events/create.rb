module Placetr::Controllers::Events
  class Create
    include Placetr::Action

    expose :event

    params do
      param :location_id, presence: true
      param :start_time, presence: true
      param :end_time, presence: false
      param :name, presence: true
      param :description, presence: false
    end

    def call(params)
      self.format = :json
      @event = EventRepository.persist(Event.new(params))
    end
  end
end
