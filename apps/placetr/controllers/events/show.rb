module Placetr::Controllers::Events
  class Show
    include Placetr::Action

    expose :event

    params do
      param :id, presence: true
    end

    def call(params)
      self.format = :json
      @event = EventRepository.find(params[:id])
    end
  end
end
