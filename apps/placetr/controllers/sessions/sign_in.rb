module Placetr::Controllers::Sessions
  class SignIn
    include Placetr::Action
    include ::BCrypt

    expose :user

    params do
      param :email, presence: true
      param :password, presence: true
    end

    def call(params)
      self.format = :json

      user = current_user(params)
      puts "params: #{params['email'].inspect}"
      if !user.encrypted_password.present? || !password_match?(user, params[:password])
        halt(400, {
          status: :error,
          message: 'Invalid password',
          params: ['password']
        }.to_json)
      end

      user.authentication_token = SecureRandom.hex
      @user = UserRepository.persist(user)
    end

    def password_match?(user, password)
      (BCrypt::Password.new(user.encrypted_password) == password)
    end

    def current_user(params)
      user = UserRepository.find_by(email: params['email']).first
      raise Lotus::Model::InvalidQueryError if user.nil?
      user
    end
  end
end
