module Placetr::Controllers::Categories
  class Index
    include Placetr::Action

    expose :categories

    params do
      param :limit, presence: true
      param :offset, presence: true
    end

    def call(params)
      self.format = :json
      @categories = CategoryRepository.paginate(params['limit'], params['offset'])
    end
  end
end
