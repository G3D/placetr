module Placetr::Controllers::Categories
  class Update
    include Placetr::Action

    expose :category

    params do
      param :id, presence: true
      param :slug, presence: false
      param :name, presence: false
    end

    def call(params)
      self.format = :json

      options = prepare_options(params)
      puts "Update category #{options.inspect}"
      @category = CategoryRepository.find(options.delete('id'))
      options.each do |key, value|
        @category.public_send("#{key}=", value)
      end
      CategoryRepository.persist(@category)
    end
  end
end
