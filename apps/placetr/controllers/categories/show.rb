module Placetr::Controllers::Categories
  class Show
    include Placetr::Action

    expose :category

    params do
      param :id, presence: true
    end

    def call(params)
      self.format = :json
      @category = CategoryRepository.find(params[:id])
    end
  end
end
