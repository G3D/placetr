module Placetr::Controllers::Categories
  class Destroy
    include Placetr::Action

    expose :category

    params do
      param :id, presence: true
    end

    def call(params)
      self.format = :json

      @category = CategoryRepository.find(params['id'])
      CategoryRepository.delete(@category)
    end
  end
end
