module Placetr::Controllers::Categories
  class Create
    include Placetr::Action

    expose :category

    params do
      param :slug, presence: true
      param :name, presence: false
    end

    def call(params)
      self.format = :json
      @category = CategoryRepository.persist(Category.new(params))
    end
  end
end
