module ModelLocation
  def in_radius(lat, lng, radius)
    query do
      where("(earth_box(ll_to_earth(#{lat}, #{lng}), #{radius}) @> ll_to_earth(locations.latitude, locations.longitude))")
    end
  end
end
