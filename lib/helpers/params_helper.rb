module ParamsHelper
  def validate!
    self.format = :json
    unless params.valid?
      halt(400, {
        status: :error,
        message: 'Invalid params',
        params: errors.map(&:attribute_name)
      }.to_json)
    end
  end

  def prepare_options(params)
    params.to_hash.keep_if {|_,v| v.present?}
  end
end
