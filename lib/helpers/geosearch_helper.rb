module GeosearchHelper
  def geosearch(options)
    return repository if !options['location']['address'].present? || !(options['location']['latitude'].present? && options['location']['longitude'].present?) 
    options['location'] = options['location'].to_hash
    if options['location'].present?
      if options['location']['address'].present?
        options['location']['latitude'], options['location']['longitude'] = Geocoder.coordinates(options['location']['address'])
      end
      repository.in_radius(
        options['location']['latitude'],
        options['location']['longitude'],
        options['radius']
      )
    else
      repository
    end
  end
end
