module ModelHelper
  def paginate(limit, offset)
    query do
      limit(limit).offset(offset)
    end
  end

  def find_by(options)
    query do
      options.each do |key, value|
        where(key => value)
      end
    end
  end
end
