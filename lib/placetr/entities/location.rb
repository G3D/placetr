class Location
  include Lotus::Entity
  # id is implicit
  attributes :user_id, :contact_id, :country_id, :latitude, :longitude, :name,
             :description, :verified, :address, :city, :created_at, :updated_at

end
