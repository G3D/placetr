class Country
  include Lotus::Entity
  # id is implicit
  attributes :name, :country_code, :created_at, :updated_at

end
