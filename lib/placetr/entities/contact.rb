class Contact
  include Lotus::Entity
  # id is implicit
  attributes :first_name, :last_name, :site, :phone, :created_at, :updated_at
end
