class Category
  include Lotus::Entity
  # id is implicit
  attributes :name, :slug, :created_at, :updated_at
end
