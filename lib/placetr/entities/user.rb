class User
  include Lotus::Entity
  # id is implicit
  attributes :email, :authentication_token, :encrypted_password, :reset_password_token,
             :current_sign_in_ip, :last_sign_in_ip, :confirmation_token, :role,
             :sign_in_count, :reset_password_sent_at, :current_sign_in_at, :last_sign_in_at,
             :confirmed_at, :confirmation_sent_at, :created_at, :updated_at
end
