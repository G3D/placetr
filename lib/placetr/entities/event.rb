class Event
  include Lotus::Entity
  # id is implicit
  attributes :location_id, :start_time, :end_time, :name,
             :description, :created_at, :updated_at

  def location
    LocationRepository.find(location_id)
  end
end
