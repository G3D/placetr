class Guide
  include Lotus::Entity
  # id is implicit
  attributes :user_id, :latitude, :longitude, :name,
             :description, :city, :created_at, :updated_at
end
