require 'lotus/model'
Dir[ "#{ __dir__ }/helpers/**/*.rb", "#{ __dir__ }/placetr/**/*.rb"].each { |file| require_relative file }

Lotus::Model.configure do
  # Database adapter
  #
  # Available options:
  #
  #  * Memory adapter
  #    adapter type: :memory, uri: 'memory://localhost/placetr_development'
  #
  #  * SQL adapter
  #    adapter type: :sql, uri: 'sqlite://db/placetr_development.sqlite3'
  #    adapter type: :sql, uri: 'postgres://localhost/placetr_development'
  #    adapter type: :sql, uri: 'mysql://localhost/placetr_development'
  #
  adapter type: :sql, uri: ENV['PLACETR_DATABASE_URL']

  ##
  # Database mapping
  #
  # Intended for specifying application wide mappings.
  #
  # You can specify mapping file to load with:
  #
  # mapping "#{__dir__}/config/mapping"
  #
  # Alternatively, you can use a block syntax like the following:
  #
  mapping do
    collection :categories do
      entity     Category
      repository CategoryRepository

      attribute :id,   String
      attribute :name, String
      attribute :slug, String
      attribute :created_at, DateTime
      attribute :updated_at, DateTime
    end

    collection :contacts do
      entity     Contact
      repository ContactRepository

      attribute :id,   String
      attribute :first_name, String
      attribute :last_name, String
      attribute :site, String
      attribute :phone, String
      attribute :created_at, DateTime
      attribute :updated_at, DateTime
    end

    collection :events do
      entity     Event
      repository EventRepository

      attribute :id,   String
      attribute :location_id, String
      attribute :start_time, DateTime
      attribute :end_time, DateTime
      attribute :name, String
      attribute :description, String
      attribute :created_at, DateTime
      attribute :updated_at, DateTime
    end

    collection :guides do
      entity     Guide
      repository GuideRepository

      attribute :id,   String
      attribute :user_id, String
      attribute :latitude, Float
      attribute :longitude, Float
      attribute :name, String
      attribute :description, String
      attribute :city, String
      attribute :created_at, DateTime
      attribute :updated_at, DateTime
    end

    collection :locations do
      entity     Location
      repository LocationRepository

      attribute :id,   String
      attribute :user_id, String
      attribute :contact_id, String
      attribute :country_id, String
      attribute :latitude, Float
      attribute :longitude, Float
      attribute :name, String
      attribute :description, String
      attribute :address, String
      attribute :city, String
      attribute :verified, Boolean
      attribute :created_at, DateTime
      attribute :updated_at, DateTime
    end

    collection :countries do
      entity     Country
      repository CountryRepository

      attribute :id,   String
      attribute :name, String
      attribute :country_code, String
      attribute :created_at, DateTime
      attribute :updated_at, DateTime
    end

    collection :users do
      entity     User
      repository UserRepository

      attribute :id,   String
      attribute :email, String
      attribute :role, String
      attribute :authentication_token, String
      attribute :encrypted_password, String
      attribute :reset_password_token, String
      attribute :current_sign_in_ip, String
      attribute :last_sign_in_ip, String
      attribute :confirmation_token, String
      attribute :sign_in_count, Integer
      attribute :reset_password_sent_at, DateTime
      attribute :current_sign_in_at, DateTime
      attribute :last_sign_in_at, DateTime
      attribute :confirmed_at, DateTime
      attribute :confirmation_sent_at, DateTime
      attribute :created_at, DateTime
      attribute :updated_at, DateTime
    end
  end
end.load!
