require 'spec_helper'
require_relative '../../../../apps/placetr/views/countries/index'

describe Placetr::Views::Countries::Index do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/countries/index.html.erb') }
  let(:view)      { Placetr::Views::Countries::Index.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
