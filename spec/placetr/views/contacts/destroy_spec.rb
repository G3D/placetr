require 'spec_helper'
require_relative '../../../../apps/placetr/views/contacts/destroy'

describe Placetr::Views::Contacts::Destroy do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/contacts/destroy.html.erb') }
  let(:view)      { Placetr::Views::Contacts::Destroy.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
