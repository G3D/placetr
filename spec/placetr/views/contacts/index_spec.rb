require 'spec_helper'
require_relative '../../../../apps/placetr/views/contacts/index'

describe Placetr::Views::Contacts::Index do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/contacts/index.html.erb') }
  let(:view)      { Placetr::Views::Contacts::Index.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
