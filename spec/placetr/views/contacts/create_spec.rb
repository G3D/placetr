require 'spec_helper'
require_relative '../../../../apps/placetr/views/contacts/create'

describe Placetr::Views::Contacts::Create do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/contacts/create.html.erb') }
  let(:view)      { Placetr::Views::Contacts::Create.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
