require 'spec_helper'
require_relative '../../../../apps/placetr/views/contacts/show'

describe Placetr::Views::Contacts::Show do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/contacts/show.html.erb') }
  let(:view)      { Placetr::Views::Contacts::Show.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
