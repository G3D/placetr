require 'spec_helper'
require_relative '../../../../apps/placetr/views/contacts/update'

describe Placetr::Views::Contacts::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/contacts/update.html.erb') }
  let(:view)      { Placetr::Views::Contacts::Update.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
