require 'spec_helper'
require_relative '../../../../apps/placetr/views/sessions/sign_out'

describe Placetr::Views::Sessions::SignOut do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/sessions/sign_out.html.erb') }
  let(:view)      { Placetr::Views::Sessions::SignOut.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
