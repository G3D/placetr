require 'spec_helper'
require_relative '../../../../apps/placetr/views/sessions/sign_in'

describe Placetr::Views::Sessions::SignIn do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/sessions/sign_in.html.erb') }
  let(:view)      { Placetr::Views::Sessions::SignIn.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
