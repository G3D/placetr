require 'spec_helper'
require_relative '../../../../apps/placetr/views/users/create'

describe Placetr::Views::Users::Create do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/users/create.html.erb') }
  let(:view)      { Placetr::Views::Users::Create.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
