require 'spec_helper'
require_relative '../../../../apps/placetr/views/users/destroy'

describe Placetr::Views::Users::Destroy do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/users/destroy.html.erb') }
  let(:view)      { Placetr::Views::Users::Destroy.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
