require 'spec_helper'
require_relative '../../../../apps/placetr/views/users/update'

describe Placetr::Views::Users::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/users/update.html.erb') }
  let(:view)      { Placetr::Views::Users::Update.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
