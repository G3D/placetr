require 'spec_helper'
require_relative '../../../../apps/placetr/views/users/show'

describe Placetr::Views::Users::Show do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/users/show.html.erb') }
  let(:view)      { Placetr::Views::Users::Show.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
