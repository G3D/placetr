require 'spec_helper'
require_relative '../../../../apps/placetr/views/users/index'

describe Placetr::Views::Users::Index do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/users/index.html.erb') }
  let(:view)      { Placetr::Views::Users::Index.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
