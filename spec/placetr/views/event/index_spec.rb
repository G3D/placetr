require 'spec_helper'
require_relative '../../../../apps/placetr/views/event/index'

describe Placetr::Views::Event::Index do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/event/index.html.erb') }
  let(:view)      { Placetr::Views::Event::Index.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
