require 'spec_helper'
require_relative '../../../../apps/placetr/views/event/show'

describe Placetr::Views::Event::Show do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/event/show.html.erb') }
  let(:view)      { Placetr::Views::Event::Show.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
