require 'spec_helper'
require_relative '../../../../apps/placetr/views/event/destroy'

describe Placetr::Views::Event::Destroy do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/event/destroy.html.erb') }
  let(:view)      { Placetr::Views::Event::Destroy.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
