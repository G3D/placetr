require 'spec_helper'
require_relative '../../../../apps/placetr/views/event/update'

describe Placetr::Views::Event::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/event/update.html.erb') }
  let(:view)      { Placetr::Views::Event::Update.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
