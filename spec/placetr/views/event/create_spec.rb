require 'spec_helper'
require_relative '../../../../apps/placetr/views/event/create'

describe Placetr::Views::Event::Create do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/event/create.html.erb') }
  let(:view)      { Placetr::Views::Event::Create.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
