require 'spec_helper'
require_relative '../../../../apps/placetr/views/guide/destroy'

describe Placetr::Views::Guide::Destroy do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/guide/destroy.html.erb') }
  let(:view)      { Placetr::Views::Guide::Destroy.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
