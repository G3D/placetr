require 'spec_helper'
require_relative '../../../../apps/placetr/views/guide/index'

describe Placetr::Views::Guide::Index do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/guide/index.html.erb') }
  let(:view)      { Placetr::Views::Guide::Index.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
