require 'spec_helper'
require_relative '../../../../apps/placetr/views/guide/show'

describe Placetr::Views::Guide::Show do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/guide/show.html.erb') }
  let(:view)      { Placetr::Views::Guide::Show.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
