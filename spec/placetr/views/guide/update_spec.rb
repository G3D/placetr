require 'spec_helper'
require_relative '../../../../apps/placetr/views/guide/update'

describe Placetr::Views::Guide::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/guide/update.html.erb') }
  let(:view)      { Placetr::Views::Guide::Update.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
