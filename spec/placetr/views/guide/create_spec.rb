require 'spec_helper'
require_relative '../../../../apps/placetr/views/guide/create'

describe Placetr::Views::Guide::Create do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/guide/create.html.erb') }
  let(:view)      { Placetr::Views::Guide::Create.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
