require 'spec_helper'
require_relative '../../../../apps/placetr/views/location/update'

describe Placetr::Views::Location::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/location/update.html.erb') }
  let(:view)      { Placetr::Views::Location::Update.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
