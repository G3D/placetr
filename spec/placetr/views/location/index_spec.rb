require 'spec_helper'
require_relative '../../../../apps/placetr/views/location/index'

describe Placetr::Views::Location::Index do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/location/index.html.erb') }
  let(:view)      { Placetr::Views::Location::Index.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
