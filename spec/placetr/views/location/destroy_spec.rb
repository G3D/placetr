require 'spec_helper'
require_relative '../../../../apps/placetr/views/location/destroy'

describe Placetr::Views::Location::Destroy do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/location/destroy.html.erb') }
  let(:view)      { Placetr::Views::Location::Destroy.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
