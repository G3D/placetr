require 'spec_helper'
require_relative '../../../../apps/placetr/views/location/show'

describe Placetr::Views::Location::Show do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/location/show.html.erb') }
  let(:view)      { Placetr::Views::Location::Show.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
