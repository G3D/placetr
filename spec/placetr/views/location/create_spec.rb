require 'spec_helper'
require_relative '../../../../apps/placetr/views/location/create'

describe Placetr::Views::Location::Create do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/location/create.html.erb') }
  let(:view)      { Placetr::Views::Location::Create.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
