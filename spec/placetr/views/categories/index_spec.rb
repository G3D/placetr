require 'spec_helper'
require_relative '../../../../apps/placetr/views/categories/index'

describe Placetr::Views::Categories::Index do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/categories/index.html.erb') }
  let(:view)      { Placetr::Views::Categories::Index.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
