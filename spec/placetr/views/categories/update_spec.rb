require 'spec_helper'
require_relative '../../../../apps/placetr/views/categories/update'

describe Placetr::Views::Categories::Update do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/categories/update.html.erb') }
  let(:view)      { Placetr::Views::Categories::Update.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
