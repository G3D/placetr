require 'spec_helper'
require_relative '../../../../apps/placetr/views/categories/destroy'

describe Placetr::Views::Categories::Destroy do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/categories/destroy.html.erb') }
  let(:view)      { Placetr::Views::Categories::Destroy.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
