require 'spec_helper'
require_relative '../../../../apps/placetr/views/categories/show'

describe Placetr::Views::Categories::Show do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/categories/show.html.erb') }
  let(:view)      { Placetr::Views::Categories::Show.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
