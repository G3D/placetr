require 'spec_helper'
require_relative '../../../../apps/placetr/views/categories/create'

describe Placetr::Views::Categories::Create do
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Lotus::View::Template.new('apps/placetr/templates/categories/create.html.erb') }
  let(:view)      { Placetr::Views::Categories::Create.new(template, exposures) }

  it "exposes #foo" do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
