require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/countries/index'

describe Placetr::Controllers::Countries::Index do
  let(:action) { Placetr::Controllers::Countries::Index.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
