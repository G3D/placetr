require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/sessions/sign_in'

describe Placetr::Controllers::Sessions::SignIn do
  let(:action) { Placetr::Controllers::Sessions::SignIn.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
