require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/sessions/sign_out'

describe Placetr::Controllers::Sessions::SignOut do
  let(:action) { Placetr::Controllers::Sessions::SignOut.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
