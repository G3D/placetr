require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/location/index'

describe Placetr::Controllers::Location::Index do
  let(:action) { Placetr::Controllers::Location::Index.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
