require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/location/destroy'

describe Placetr::Controllers::Location::Destroy do
  let(:action) { Placetr::Controllers::Location::Destroy.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
