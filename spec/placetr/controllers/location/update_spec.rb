require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/location/update'

describe Placetr::Controllers::Location::Update do
  let(:action) { Placetr::Controllers::Location::Update.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
