require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/location/create'

describe Placetr::Controllers::Location::Create do
  let(:action) { Placetr::Controllers::Location::Create.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
