require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/location/show'

describe Placetr::Controllers::Location::Show do
  let(:action) { Placetr::Controllers::Location::Show.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
