require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/users/update'

describe Placetr::Controllers::Users::Update do
  let(:action) { Placetr::Controllers::Users::Update.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
