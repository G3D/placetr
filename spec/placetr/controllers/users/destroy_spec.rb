require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/users/destroy'

describe Placetr::Controllers::Users::Destroy do
  let(:action) { Placetr::Controllers::Users::Destroy.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
