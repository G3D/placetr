require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/users/index'

describe Placetr::Controllers::Users::Index do
  let(:action) { Placetr::Controllers::Users::Index.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
