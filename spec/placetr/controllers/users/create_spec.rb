require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/users/create'

describe Placetr::Controllers::Users::Create do
  let(:action) { Placetr::Controllers::Users::Create.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
