require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/users/show'

describe Placetr::Controllers::Users::Show do
  let(:action) { Placetr::Controllers::Users::Show.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
