require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/categories/update'

describe Placetr::Controllers::Categories::Update do
  let(:action) { Placetr::Controllers::Categories::Update.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
