require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/categories/create'

describe Placetr::Controllers::Categories::Create do
  let(:action) { Placetr::Controllers::Categories::Create.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
