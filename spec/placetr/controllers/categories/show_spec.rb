require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/categories/show'

describe Placetr::Controllers::Categories::Show do
  let(:action) { Placetr::Controllers::Categories::Show.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
