require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/categories/destroy'

describe Placetr::Controllers::Categories::Destroy do
  let(:action) { Placetr::Controllers::Categories::Destroy.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
