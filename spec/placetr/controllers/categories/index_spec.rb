require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/categories/index'

describe Placetr::Controllers::Categories::Index do
  let(:action) { Placetr::Controllers::Categories::Index.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
