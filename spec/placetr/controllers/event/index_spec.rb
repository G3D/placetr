require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/event/index'

describe Placetr::Controllers::Event::Index do
  let(:action) { Placetr::Controllers::Event::Index.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
