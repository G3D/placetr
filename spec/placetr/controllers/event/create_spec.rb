require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/event/create'

describe Placetr::Controllers::Event::Create do
  let(:action) { Placetr::Controllers::Event::Create.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
