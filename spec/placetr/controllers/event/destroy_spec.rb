require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/event/destroy'

describe Placetr::Controllers::Event::Destroy do
  let(:action) { Placetr::Controllers::Event::Destroy.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
