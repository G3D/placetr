require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/event/show'

describe Placetr::Controllers::Event::Show do
  let(:action) { Placetr::Controllers::Event::Show.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
