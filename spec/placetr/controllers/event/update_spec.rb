require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/event/update'

describe Placetr::Controllers::Event::Update do
  let(:action) { Placetr::Controllers::Event::Update.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
