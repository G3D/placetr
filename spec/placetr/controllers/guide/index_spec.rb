require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/guide/index'

describe Placetr::Controllers::Guide::Index do
  let(:action) { Placetr::Controllers::Guide::Index.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
