require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/guide/destroy'

describe Placetr::Controllers::Guide::Destroy do
  let(:action) { Placetr::Controllers::Guide::Destroy.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
