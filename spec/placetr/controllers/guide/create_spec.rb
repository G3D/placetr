require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/guide/create'

describe Placetr::Controllers::Guide::Create do
  let(:action) { Placetr::Controllers::Guide::Create.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
