require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/guide/show'

describe Placetr::Controllers::Guide::Show do
  let(:action) { Placetr::Controllers::Guide::Show.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
