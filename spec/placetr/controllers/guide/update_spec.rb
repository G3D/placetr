require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/guide/update'

describe Placetr::Controllers::Guide::Update do
  let(:action) { Placetr::Controllers::Guide::Update.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
