require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/contacts/create'

describe Placetr::Controllers::Contacts::Create do
  let(:action) { Placetr::Controllers::Contacts::Create.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
