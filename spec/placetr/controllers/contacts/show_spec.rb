require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/contacts/show'

describe Placetr::Controllers::Contacts::Show do
  let(:action) { Placetr::Controllers::Contacts::Show.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
