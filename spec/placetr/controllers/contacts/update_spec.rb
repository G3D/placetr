require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/contacts/update'

describe Placetr::Controllers::Contacts::Update do
  let(:action) { Placetr::Controllers::Contacts::Update.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
