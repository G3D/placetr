require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/contacts/destroy'

describe Placetr::Controllers::Contacts::Destroy do
  let(:action) { Placetr::Controllers::Contacts::Destroy.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
