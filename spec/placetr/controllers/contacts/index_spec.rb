require 'spec_helper'
require_relative '../../../../apps/placetr/controllers/contacts/index'

describe Placetr::Controllers::Contacts::Index do
  let(:action) { Placetr::Controllers::Contacts::Index.new }
  let(:params) { Hash[] }

  it "is successful" do
    response = action.call(params)
    expect(response[0]).to eq 200
  end
end
